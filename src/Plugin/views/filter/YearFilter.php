<?php

namespace Drupal\year_filter\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Filter to handle dates stored as a timestamp.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("year_filter")
 */
class YearFilter extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->definition['options callback'] = [$this, 'generateOptions'];
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if (!empty($this->options['exposed']) && $this->options['exposed']) {
      return $this->t('exposed');
    }

    return $this->value['year'] ?? $this->t('- Select -');
  }

  /**
   * Helper function that generates the options.
   */
  public function defaultOptions($min = NULL, $max = NULL) {
    $current_year = date('Y');
    $options      = [];

    $min_year = 1900;
    if (!empty($min)) {
      $min_year = $min;
    }

    $max_year = $current_year;
    if (!empty($max)) {
      $max_year = $max;
    }

    for ($year = $min_year; $year <= $max_year; $year++) {
      $options[$year] = $year;
    }

    return $options;
  }

  public function defaultFormOptions($min = NULL, $max = NULL) {
    return [NULL => $this->t('- Select an option -')] + $this->defaultOptions($min, $max);
  }

  public function generateOptions($min = NULL, $max = NULL) {
    return $this->defaultOptions($min, $max);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options["value"]["type"] = 'select';

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $options = $this->defaultFormOptions();

    if (!$this->options['exposed']) {
      $form['value']['year'] = [
        '#type'          => 'select',
        '#title'         => $this->t('Year'),
        '#options'       => $options,
        '#default_value' => $this->value['year'] ?? NULL,
      ];
    }
    else {
      $form['value']['type'] = [
        '#type'          => 'select',
        '#title'         => $this->t('Widget type'),
        '#options'       => [
          'select' => $this->t('Select'),
          'radios' => $this->t('Radios'),
          'checkboxes' => $this->t('Checkboxes'),
        ],
        '#default_value' => $this->value['type'] ?? 'select',
      ];

      $form['value']['min'] = [
        '#type'          => 'select',
        '#title'         => $this->t('Min'),
        '#options'       => $this->defaultFormOptions(),
        '#default_value' => $this->value['min'] ?? NULL,
      ];

      $form['value']['max'] = [
        '#type'          => 'select',
        '#title'         => $this->t('Max'),
        '#options'       => $this->defaultFormOptions(),
        '#default_value' => $this->value['max'] ?? NULL,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state) {
    $options = $this->generateOptions($this->value['min'], $this->value['max']);
    if (!$this->options["expose"]["required"]) {
      $options = ['All' => t('- Any -')] + $options;
    }

    $form[$this->getPluginId()] = [
      '#type'    => $this->value['type'],
      '#options' => $options,
    ];
  }

  /**
   *
   */
  public function acceptExposedInput($input) {
    return TRUE;
  }

  /**
   *
   */
  public function storeExposedInput($input, $status) {
    if (!empty($input[$this->getPluginId()])) {
      $this->value['year'] = $input[$this->getPluginId()];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    if (!empty($this->value['year']) && $this->value['year'] !== 'All') {
      /** @var \Drupal\views\Plugin\views\query\Sql $query */
      $query = $this->query;

      $table  = $this->configuration["entity_type"] . '__' . $this->configuration["field"];
      $column = $this->configuration["field"] . '_value';

      $value = $this->value['year'];
      if (is_array($this->value['year'])) {
        foreach ($this->value['year'] as $year) {
          if ($year !== 0) {
            $values[$year] = $year;
          }
        }

        if (!empty($values)) {
          $value = implode(',', $values);
          // @todo checkboxes needs review, not working atm.
          $query->addWhereExpression(0, "YEAR({$table}.{$column}) IN (:year)", [':year' => $value]);
        }
      }
      else {
        $query->addWhereExpression(0, "YEAR({$table}.{$column}) = :year", [':year' => $value]);
      }

      $this->query = $query;
    }
  }

}

