<?php

namespace Drupal\year_filter\Plugin\views\filter;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter to handle dates stored as a timestamp.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("year_filter_selective")
 */
class YearFilterSelective extends YearFilter implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Filter constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database')
    );
  }

  /**
   * Helper function that generates the options.
   */
  public function generateOptions($min = NULL, $max = NULL) {
    $table = $this->definition["entity_type"] . '__' . $this->definition["field"];
    $column = $this->definition["field"] . '_value';
    $query = $this->database->select($table, 'a');
    $query->addExpression("YEAR({$column})", 'year');

    $query->groupBy('year');
    $results = $query->execute()
      ->fetchCol();

    $current_year = date('Y');
    $options      = [];

    $min_year = 1900;
    if (!empty($min)) {
      $min_year = $min;
    }

    $max_year = $current_year;
    if (!empty($max)) {
      $max_year = $max;
    }

    for ($year = $min_year; $year <= $max_year; $year++) {
      if (in_array($year, $results)) {
        $options[$year] = $year;
      }
    }

    return $options;
  }

}

